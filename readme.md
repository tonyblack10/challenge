# Product CRUD Challenge

## Technologies Used
- PHP 7.1;
- Laravel 5.6;
- MySQL Server 5.7.21;
- Bootstrap 4 (CSS).

## Run Project 
- a database must be created for the project
- run: *composer install*
- create the .env file (based on file .env.example) and adjust the environment variables
- run: *php artisan migrate* (generate database tables)
- run: *php artisan db:seed* (generate records to tags table)
- run: *php artisan server* (start development server)

Note: **the commands must be executed in the project folder**.