<?php

namespace challenge;

use Illuminate\Database\Eloquent\Model;

use challenge\Tag;

class Product extends Model
{
    public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = ['title', 'description', 'image', 'stock'];

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'products_tags', 'product_id', 'tag_id');
    }
}
