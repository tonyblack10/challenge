<?php

namespace challenge\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:6|max:255',
            'description' => 'max:4000',
            'stock' => 'required|integer|min:1',
            'image' => 'required|mimes:jpeg,gif,png|max:5120',
        ];
    }
}
