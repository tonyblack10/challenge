<?php

namespace challenge\Http\Controllers;

use Illuminate\Http\Request;

use challenge\Http\Requests\SaveProduct;
use challenge\Repositories\ProductRepository;
use challenge\Repositories\TagRepository;
use challenge\Product;
use challenge\Helpers\FileUploadHelper;

class ProductController extends Controller
{
    protected $products;
    protected $uploader;
    protected $tags;

    public function __construct(ProductRepository $products, FileUploadHelper $uploader, 
        TagRepository $tags)
    {
        $this->products = $products;
        $this->uploader = $uploader;
        $this->tags = $tags;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('products.index', ['products' => $this->products->findAll()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.form', 
            ['product' => new Product(), 'tags' => $this->tags->findAll(), 'tagsSelected' => null]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  SaveProduct  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveProduct $request)
    {
        $data = $request->all();
        $data['image'] = $this->uploader->save($request->image, 'images');

        $this->products->save($data);

        return redirect()
            ->route('products.index')
            ->with('success', 'Product created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = $this->products->findById($id);
        $tagsSelected = $product->tags->pluck('id');
        return view('products.form', 
            ['product' => $product, 'tags' => $this->tags->findAll(), 'tagsSelected' => $tagsSelected]);

        // return view('products.form', ['product' => $this->products->findById($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  SaveProduct  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SaveProduct $request, $id)
    {
        $data = $request->all();
        $data['image'] = $this->uploader->save($request->image, 'images');
        $this->products->update($data, $id);

        return redirect()
            ->route('products.index')
            ->with('success', 'Product successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = $this->products->findById($id);
        $imageIsDeleted = $this->uploader->delete($product->image, 'images');

        $this->products->remove($product);

        return response(['success' => 'Product has been deleted!', 'deleteImage' => $imageIsDeleted], 201);
    }
}
