<?php

namespace challenge\Http\Controllers;

use Illuminate\Http\Request;

use challenge\Repositories\TagRepository;

class HomeController extends Controller
{
    protected $tags;
    
    public function __construct(TagRepository $tags)
    {
        $this->tags = $tags;
    }

    public function index()
    {
        return view('home', ['tags' => $this->tags->findMostUsed()]);
    }

}
