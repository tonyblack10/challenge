<?php

namespace challenge\Repositories;

use challenge\Product;

class ProductRepository
{
    public function findAll()
    {
        return Product::orderBy('title', 'asc')->paginate(10);
    }

    public function save(array $data)
    {
        $product = Product::create($data);
        if(array_key_exists('tags', $data)) {
            $product->tags()->attach($data['tags']);
        }
    }

    public function findById(int $id): Product
    {
        return Product::find($id);
    }

    public function update(array $data, int $id)
    {
        $product = Product::find($id);
        $product->title = $data['title'];
        $product->description = $data['description'];
        $product->image = $data['image'];
        $product->stock = $data['stock'];

        $product->save();

        if(array_key_exists('tags', $data)) {
            $product->tags()->sync($data['tags']);
        }
    }

    public function remove(Product $product)
    {
        $product->delete();
    }
}
