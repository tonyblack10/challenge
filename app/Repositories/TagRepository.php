<?php

namespace challenge\Repositories;

use challenge\Tag;

class TagRepository
{
    public function findAll()
    {
        $tags = Tag::orderBy('name', 'asc')->get();

        return $tags->pluck('name', 'id');
    }

    public function findMostUsed(int $limit = 10)
    {
        return Tag::withCount('products')
            ->orderBy('products_count', 'desc')
            ->orderBy('name', 'asc')
            ->limit(10)
            ->get();
    }

}
