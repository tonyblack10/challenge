<?php

namespace challenge\Helpers;

use Illuminate\Filesystem\Filesystem;

class FileUploadHelper
{
    protected $files;

    public function __construct(Filesystem $files)
    {
        $this->files = $files;
    }

    public function save($file, string $folder): string
    {
        $filename = time().'.'.$file->getClientOriginalExtension();
        $file->storeAs('public/'.$folder, $filename);

        return $filename;
    }

    public function delete($file, string $folder)
    {
        $path = public_path().'/storage/'.$folder.'/';

        return $this->files->delete($path.$file);
    }
}
