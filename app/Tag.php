<?php

namespace challenge;

use Illuminate\Database\Eloquent\Model;

use challenge\Product;

class Tag extends Model
{
    protected $fillable = ['name'];
    public $timestamps = false;

    public function products()
    {
        return $this->belongsToMany(Product::class, 'products_tags', 'tag_id', 'product_id');
    }
}
