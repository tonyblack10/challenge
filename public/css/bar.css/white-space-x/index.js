/**
 * @file List of ECMAScript white space characters.
 * @version 3.0.0
 * @author Xotic750 <Xotic750@gmail.com>
 * @copyright  Xotic750
 * @license {@link <https://opensource.org/licenses/MIT> MIT}
 * @module white-space-x
 */

'use strict';

/**
 * A record of a white space character.
 *
 * @typedef {Object} CharRecord
 * @property {number} code - The character code.
 * @property {string} description - A description of the character.
 * @property {boolean} es5 - Whether the spec lists this as a white space.
 * @property {boolean} es2015 - Whether the spec lists this as a white space.
 * @property {boolean} es2016 - Whether the spec lists this as a white space.
 * @property {boolean} es2017 - Whether the spec lists this as a white space.
 * @property {boolean} es2018 - Whether the spec lists this as a white space.
 * @property {string} string - The character string.
 */

/**
 * An array of the whitespace char codes, string, descriptions and language
 * presence in the specifications.
 *
 * @private
 * @type Array.<CharRecord>
 */
var list = [
  {
    code: 0x0009,
    description: 'Tab',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: true,
    es2018: true,
    string: '\u0009'
  },
  {
    code: 0x000a,
    description: 'Line Feed',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: true,
    es2018: true,
    string: '\u000a'
  },
  {
    code: 0x000b,
    description: 'Vertical Tab',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: true,
    es2018: true,
    string: '\u000b'
  },
  {
    code: 0x000c,
    description: 'Form Feed',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: true,
    es2018: true,
    string: '\u000c'
  },
  {
    code: 0x000d,
    description: 'Carriage Return',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: true,
    es2018: true,
    string: '\u000d'
  },
  {
    code: 0x0020,
    description: 'Space',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: true,
    es2018: true,
    string: '\u0020'
  },
  /*
  {
    code: 0x0085,
    description: 'Next line',
    es5: false,
    es2015: false,
    es2016: false,
    es2017: false,
    es2018: false,
    string: '\u0085'
  }
  */
  {
    code: 0x00a0,
    description: 'No-break space',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: true,
    es2018: true,
    string: '\u00a0'
  },
  {
    code: 0x1680,
    description: 'Ogham space mark',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: true,
    es2018: true,
    string: '\u1680'
  },
  {
    code: 0x180e,
    description: 'Mongolian vowel separator',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: false,
    es2018: false,
    string: '\u180e'
  },
  {
    code: 0x2000,
    description: 'En quad',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: true,
    es2018: true,
    string: '\u2000'
  },
  {
    code: 0x2001,
    description: 'Em quad',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: true,
    es2018: true,
    string: '\u2001'
  },
  {
    code: 0x2002,
    description: 'En space',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: true,
    es2018: true,
    string: '\u2002'
  },
  {
    code: 0x2003,
    description: 'Em space',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: true,
    es2018: true,
    string: '\u2003'
  },
  {
    code: 0x2004,
    description: 'Three-per-em space',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: true,
    es2018: true,
    string: '\u2004'
  },
  {
    code: 0x2005,
    description: 'Four-per-em space',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: true,
    es2018: true,
    string: '\u2005'
  },
  {
    code: 0x2006,
    description: 'Six-per-em space',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: true,
    es2018: true,
    string: '\u2006'
  },
  {
    code: 0x2007,
    description: 'Figure space',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: true,
    es2018: true,
    string: '\u2007'
  },
  {
    code: 0x2008,
    description: 'Punctuation space',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: true,
    es2018: true,
    string: '\u2008'
  },
  {
    code: 0x2009,
    description: 'Thin space',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: true,
    es2018: true,
    string: '\u2009'
  },
  {
    code: 0x200a,
    description: 'Hair space',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: true,
    es2018: true,
    string: '\u200a'
  },
  /*
  {
    code: 0x200b,
    description: 'Zero width space',
    es5: false,
    es2015: false,
    es2016: false,
    es2017: false,
    es2018: false,
    string: '\u200b'
  },
  */
  {
    code: 0x2028,
    description: 'Line separator',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: true,
    es2018: true,
    string: '\u2028'
  },
  {
    code: 0x2029,
    description: 'Paragraph separator',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: true,
    es2018: true,
    string: '\u2029'
  },
  {
    code: 0x202f,
    description: 'Narrow no-break space',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: true,
    es2018: true,
    string: '\u202f'
  },
  {
    code: 0x205f,
    description: 'Medium mathematical space',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: true,
    es2018: true,
    string: '\u205f'
  },
  {
    code: 0x3000,
    description: 'Ideographic space',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: true,
    es2018: true,
    string: '\u3000'
  },
  {
    code: 0xfeff,
    description: 'Byte Order Mark',
    es5: true,
    es2015: true,
    es2016: true,
    es2017: true,
    es2018: true,
    string: '\ufeff'
  }
];

var stringES2016 = '';
var stringES2018 = '';
var length = list.length;
for (var i = 0; i < length; i += 1) {
  if (list[i].es2016) {
    stringES2016 += list[i].string;
  }

  if (list[i].es2018) {
    stringES2018 += list[i].string;
  }
}

module.exports = {
  /**
   * An array of the whitespace char codes, string, descriptions and language
   * presence in the specifications.
   *
   * @type Array.<CharRecord>
   * @example
   * var whiteSpace = require('white-space-x');
   * whiteSpaces.list.foreach(function (item) {
   *   console.log(lib.description, item.code, item.string);
   * });
   */
  list: list,
  /**
   * A string of the ES2017 to ES2018 whitespace characters.
   *
   * @type string
   */
  string: stringES2018,

  /**
   * A string of the ES5 to ES2016 whitespace characters.
   *
   * @type string
   */
  string5: stringES2016,

  /**
   * A string of the ES5 to ES2016 whitespace characters.
   *
   * @type string
   */
  string2015: stringES2016,

  /**
   * A string of the ES5 to ES2016 whitespace characters.
   *
   * @type string
   * @example
   * var whiteSpace = require('white-space-x');
   * var characters = [
   *   '\u0009',
   *   '\u000a',
   *   '\u000b',
   *   '\u000c',
   *   '\u000d',
   *   '\u0020',
   *   '\u00a0',
   *   '\u1680',
   *   '\u180e',
   *   '\u2000',
   *   '\u2001',
   *   '\u2002',
   *   '\u2003',
   *   '\u2004',
   *   '\u2005',
   *   '\u2006',
   *   '\u2007',
   *   '\u2008',
   *   '\u2009',
   *   '\u200a',
   *   '\u2028',
   *   '\u2029',
   *   '\u202f',
   *   '\u205f',
   *   '\u3000',
   *   '\ufeff'
   * ];
   * var ws = characters.join('');
   * var re1 = new RegExp('^[' + whiteSpace.string2016 + ']+$)');
   * re1.test(ws); // true
   */
  string2016: stringES2016,

  /**
   * A string of the ES2017 to ES2018 whitespace characters.
   *
   * @type string
   */
  string2017: stringES2018,

  /**
   * A string of the ES2017 to ES2018 whitespace characters.
   *
   * @type string
   * @example
   * var whiteSpace = require('white-space-x');
   * var characters = [
   *   '\u0009',
   *   '\u000a',
   *   '\u000b',
   *   '\u000c',
   *   '\u000d',
   *   '\u0020',
   *   '\u00a0',
   *   '\u1680',
   *   '\u2000',
   *   '\u2001',
   *   '\u2002',
   *   '\u2003',
   *   '\u2004',
   *   '\u2005',
   *   '\u2006',
   *   '\u2007',
   *   '\u2008',
   *   '\u2009',
   *   '\u200a',
   *   '\u2028',
   *   '\u2029',
   *   '\u202f',
   *   '\u205f',
   *   '\u3000',
   *   '\ufeff'
   * ];
   * var ws = characters.join('');
   * var re1 = new RegExp('^[' + whiteSpace.string2018 + ']+$)');
   * re1.test(ws); // true
   */
  string2018: stringES2018
};
