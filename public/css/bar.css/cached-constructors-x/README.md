<a href="https://travis-ci.org/Xotic750/cached-constructors-x"
   title="Travis status">
<img
   src="https://travis-ci.org/Xotic750/cached-constructors-x.svg?branch=master"
   alt="Travis status" height="18"/>
</a>
<a href="https://david-dm.org/Xotic750/cached-constructors-x"
   title="Dependency status">
<img src="https://david-dm.org/Xotic750/cached-constructors-x.svg"
   alt="Dependency status" height="18"/>
</a>
<a href="https://david-dm.org/Xotic750/cached-constructors-x#info=devDependencies"
   title="devDependency status">
<img src="https://david-dm.org/Xotic750/cached-constructors-x/dev-status.svg"
   alt="devDependency status" height="18"/>
</a>
<a href="https://badge.fury.io/js/cached-constructors-x" title="npm version">
<img src="https://badge.fury.io/js/cached-constructors-x.svg"
   alt="npm version" height="18"/>
</a>
<a name="module_cached-constructors-x"></a>

## cached-constructors-x
Constructors cached from literals.

**Version**: 1.0.0  
**Author**: Xotic750 <Xotic750@gmail.com>  
**License**: [MIT](&lt;https://opensource.org/licenses/MIT&gt;)  
**Copyright**: Xotic750  
<a name="exp_module_cached-constructors-x--module.exports"></a>

### `module.exports` : <code>Object</code> ⏏
Constructors cached from literals.

**Kind**: Exported member  
**Example**  
```js
var constructors = require('cached-constructors-x');
```
