<a href="https://travis-ci.org/Xotic750/infinity-x"
   title="Travis status">
<img
   src="https://travis-ci.org/Xotic750/infinity-x.svg?branch=master"
   alt="Travis status" height="18"/>
</a>
<a href="https://david-dm.org/Xotic750/infinity-x"
   title="Dependency status">
<img src="https://david-dm.org/Xotic750/infinity-x.svg"
   alt="Dependency status" height="18"/>
</a>
<a href="https://david-dm.org/Xotic750/infinity-x#info=devDependencies"
   title="devDependency status">
<img src="https://david-dm.org/Xotic750/infinity-x/dev-status.svg"
   alt="devDependency status" height="18"/>
</a>
<a href="https://badge.fury.io/js/infinity-x" title="npm version">
<img src="https://badge.fury.io/js/infinity-x.svg"
   alt="npm version" height="18"/>
</a>
<a name="module_infinity-x"></a>

## infinity-x
The constant value Infinity.

**Version**: 1.0.0  
**Author**: Xotic750 <Xotic750@gmail.com>  
**License**: [MIT](&lt;https://opensource.org/licenses/MIT&gt;)  
**Copyright**: Xotic750  
<a name="exp_module_infinity-x--module.exports"></a>

### `module.exports` : <code>number</code> ⏏
The constant value Infinity derived mathematically by 1 / 0.

**Kind**: Exported member  
**Example**  
```js
var INFINITY = require('infinity-x');

INFINITY === Infinity; // true
-INFINITY === -Infinity; // true
INFINITY === -Infinity; // false
```
