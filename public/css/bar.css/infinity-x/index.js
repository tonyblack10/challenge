/**
 * @file The constant value Infinity.
 * @version 1.0.0
 * @author Xotic750 <Xotic750@gmail.com>
 * @copyright  Xotic750
 * @license {@link <https://opensource.org/licenses/MIT> MIT}
 * @module infinity-x
 */

'use strict';

/**
 * The constant value Infinity derived mathematically by 1 / 0.
 *
 * @type number
 * @example
 * var INFINITY = require('infinity-x');
 *
 * INFINITY === Infinity; // true
 * -INFINITY === -Infinity; // true
 * INFINITY === -Infinity; // false
 */
module.exports = 1 / 0;
