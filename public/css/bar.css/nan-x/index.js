/**
 * @file The constant NaN derived mathematically by 0 / 0.
 * @version 1.0.0
 * @author Xotic750 <Xotic750@gmail.com>
 * @copyright  Xotic750
 * @license {@link <https://opensource.org/licenses/MIT> MIT}
 * @module nan-x
 */

'use strict';

/**
 * The constant NaN derived mathematically by 0 / 0.
 *
 * @type number
 * @example
 * var NAN = require('nan-x');
 *
 * NAN !== NAN; // true
 * NAN === NAN; // false
 */
module.exports = 0 / 0;
