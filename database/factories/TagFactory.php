<?php

use Faker\Generator as Faker;

$factory->define(challenge\Tag::class, function (Faker $faker) {

    return [
        'name' => $faker->word()
    ];
});
