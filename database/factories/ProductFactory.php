<?php

use Faker\Generator as Faker;

$factory->define(challenge\Product::class, function (Faker $faker) {

    return [
        'title' => $faker->text($maxNbChars = 20),
        'description' => $faker->text($maxNbChars = 300),
        'stock' => $faker->randomNumber($nbDigits = 2),
        'image' => $faker->image('public/storage/images', 400, 300, null, false)
    ];
});
