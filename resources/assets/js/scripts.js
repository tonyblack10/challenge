$(document).ready(function() {
    $('.btn-delete').click(function(e) {
      e.preventDefault();
      var _token = $('meta[name=csrf-token]').attr("content");
      var	product	=	$(this).closest(".product");
      swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this product!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $.ajax({
            url: $(this).attr('href'),
            type:	'POST',
            data:	{	_method:	'DELETE', _token	}
          }).done(function(data,	textStatus,	jqXHR)	{
            product.fadeOut();
            swal("Product has been deleted!", {
              icon: "success",
            });
            console.log(data);
          }).fail(function(jqXHR,	textStatus,	errorThrown)	{
            swal("Error while trying to delete product!", {
              icon: "danger",
            });
          });
        }
      });
    });

    $('#tags').selectize({
        persist: false,
        maxItems: null,
    });
});