@extends('layouts.app')

@section('title', 'Products List')

@section('content')
    <div class="card">
        <div class="card-header">
            Products
        </div>
        <div class="card-body">
            <h5 class="card-title">
                {{ empty($product->id) ? 'New Product' : 'Edit Product' }}
            </h5>
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(empty($product->id))
                <form method="post" action="{{ route('products.store') }}" enctype="multipart/form-data" novalidate>
            @else
                <form method="post" action="{{ route('products.update', $product->id) }}" enctype="multipart/form-data" novalidate>
                @method('PUT')
            @endif
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" name="title" id="title" required class="form-control {{ $errors->first('title') ? 'is-invalid' : '' }}"
                            value="{{ old('title') ? old('title') : $product->title }}" autocomplete="off">
                        <div class="invalid-feedback">
                            {{$errors->first('title')}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" class="form-control {{ $errors->first('description') ? 'is-invalid' : '' }}"
                            id="description" rows="3">{{ old('description') ? old('description') : $product->description }}</textarea>
                        <div class="invalid-feedback">
                            {{$errors->first('description')}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="stock">Stock</label>
                        <input type="number" name="stock" id="stock" min="1" required 
                        class="form-control {{ $errors->first('stock') ? 'is-invalid' : '' }}"
                            value="{{ old('stock') ? old('stock') : $product->stock }}">
                        <div class="invalid-feedback">
                            {{$errors->first('stock')}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="image">Image</label>
                        <input type="file" name="image" id="image" class="form-control {{ $errors->first('image') ? 'is-invalid' : '' }}">
                        <div class="invalid-feedback">
                            {{$errors->first('image')}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tags">Tags</label>
                        {!! Form::select('tags[]', $tags, $tagsSelected, ['placeholder' => '--Select--', 
                          'id' => 'tags', 'multiple' => 'multiple']) !!}
                    </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
        </div>
    </div>
@endsection