@extends('layouts.app')

@section('title', 'Products List')

@section('content')
    <div class="card">
        <div class="card-header">
            Products
        </div>
        <div class="card-body">
            <h5 class="card-title">Products List</h5>
            @include('layouts.alerts')
            <div class="row">
                <div class="col-md-3 offset-md-9 btn-add">
                    <a href="{{ route('products.create') }}" title="New Product" 
                        class="btn btn-primary btn-block">
                        New
                    </a>
                </div>
            </div>
            @if(count($products) > 0)
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th class="text-center">Image</th>
                            <th class="text-center">Stock</th>
                            <th class="text-center">
                                
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($products as $product)
                            <tr class="product">
                                <td class="align-middle">{{ $product->title }}</td>
                                <td class="text-center">
                                    <img class="product-img" src="{{ url('storage/images/'.$product->image) }}" title="{{ $product->title }}">
                                </td>
                                <td class="text-center align-middle">{{ $product->stock }}</td>
                                <td class="text-center align-middle">
                                    <a href="{{ route('products.edit', $product->id) }}">Edit</a>
                                    <a href="{{ route('products.destroy', $product->id) }}" class="btn-delete">
                                        Remove
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $products->links() }}
            @else
                <p>No records found.</p>
            @endif
        </div>
    </div>    
@endsection