@extends('layouts.app')

@section('title', 'Home')

@section('content')
    <div class="jumbotron">
        <h1 class="display-4">Product CRUD Challenge</h1>
        <hr class="my-4">
        <div class="row">
            <div class="col-md-6">
                <h4>Most Used Tags </h4>
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tags as $tag)
                            @if($tag->products_count > 0)
                                <tr>
                                    <td class="text-center">{{ $loop->iteration }}</td>
                                    <td class="text-center">{{ $tag->name }}</td>
                                    <td class="text-center">{{ $tag->products_count }}</td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection